package org.academiadecodigo.survival;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloService{

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public String sayHello(){
     return "hello";
    }

}
